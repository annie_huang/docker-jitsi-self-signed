# docker-jitsi-self-signed

Based on docker-jitsi-meet to create jitsi.meet with self-signed certificate.

This repo contains configuration to adapt the jitsi.meet for local deployment in local area network.
The key and certificate is generated and can be self-signed, added as trusted certificate.

To spin up the services, run shell scripts in the following order:
## 1. RUN "./00-cloneJitsi.sh":
Clone offical jitsi meet docker base configuration (docker-jitsi-meet folder will be created)
## 2. RUN "./01-copyFiles.sh":
To copy adjusted configuration to override files in docker-jitsi-meet folder, and generate passwords for services.
Then, it generates a random key/certificate pair. (Fill-in information is optional and you can leave it blank)
## 3. [OPTIONAL] Copy your own key/certificate in docker-jitsi-meet/certs, named them host.key/host.crt respectively to replace the auto-geneated.
If you don't already have them, you can use the pair generated by "./genKeyPair.sh" in step 2.
## 4. RUN "./02-editAddress.sh":
This file do nothing but notify user to edit .env for DOCKER_HOST_ADDRESS, it has to be set to your server LAN IP.
## 5. RUN "cd docker-jitsi-meet/" and RUN "docker-compose up -d"

Now the services are ready! The devices in the same LAN can connect to DOCKER_HOST_ADDRESS for jitsi meeting!
For beginners, use laptop/PC with firefox to browse the server, so that firefox's setting can easily bypass the problem "certificate is not trusted".
Different devices have different ways to be set to trust the certificate or trust the signing authority(you as your own CA).
