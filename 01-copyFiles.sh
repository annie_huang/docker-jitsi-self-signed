cp -r ./override/certs/ ./docker-jitsi-meet/
cp ./override/docker-compose.yml ./docker-jitsi-meet/
cp ./override/nginx.conf ./docker-jitsi-meet/
cp ./override/env.example.selfsigned ./docker-jitsi-meet/.env
cd ./docker-jitsi-meet/
./gen-passwords.sh
cd certs/
./genKeyPair.sh
